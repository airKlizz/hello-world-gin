FROM golang:alpine AS build
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o hello-world-gin

FROM alpine:latest
WORKDIR /app
COPY --from=build /app/hello-world-gin .
EXPOSE 8080
CMD ["./hello-world-gin"]
