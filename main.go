package main

import (
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		parsedURL, err := url.ParseRequestURI(c.Request.RequestURI)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		queryParams := parsedURL.Query()
		ip, _, err := net.SplitHostPort(c.Request.RemoteAddr)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		userAgent := c.Request.UserAgent()

		data := gin.H{
			"message":              "Hello, world!",
			"ip":                   ip,
			"path":                 parsedURL.Path,
			"query":                queryParams,
			"userAgent":            userAgent,
			"timestamp":            time.Now().UTC().Format(time.RFC3339),
			"environmentVariables": os.Environ(),
		}

		c.JSON(http.StatusOK, data)
	})

	router.Run(":8080")
}
